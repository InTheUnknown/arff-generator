package functions;

import java.util.concurrent.ThreadLocalRandom;

/**
 * GenerateValues
 */
public class GenerateValues {

    private int generateRandomValue(int min, int max){
        return ThreadLocalRandom.current().nextInt(min, max);
    }

    public String generateDOB(){
        StringBuilder sb = new StringBuilder();

        sb.append(generateRandomValue(1920, 2000));
        sb.append("-");
        sb.append(generateRandomValue(1, 12));
        sb.append("-");
        sb.append(generateRandomValue(1, 30));

        return sb.toString();
    }

    public int generateBMI(int min, int max){
        return generateRandomValue(min, max);
    }

    public String generateGender(){
        int g = generateRandomValue(1, 4);

        switch(g){
            case 1:
                return "Male";

            case 2:
                return "Female";

            default:
            case 3:
                return "Other";
        }
    }

    public int generateBPSys(int min, int max){
        return generateRandomValue(min, max);
    }

    public int generateBPDia(int min, int max){
        return generateRandomValue(min, max);
    }

    public int generateGlucose(int min, int max){
        return generateRandomValue(min, max);
    }

    public String generateSymptomMajor(){
        int v = generateRandomValue(0, 20);

        if(v>=0 && v<=15){
            return "Yes";
        }
        else{
            return "No";
        }
    }

    public String generateSymptomMinor(){
        int v = generateRandomValue(0, 20);

        if(v>=0 && v<=15){
            return "No";
        }
        else{
            return "Yes";
        }
    }

}