package functions;

import dataobjects.Patient;
import diseases.ChronicHeartDisease;
import diseases.Diabetes;
import diseases.Normal;
import diseases.Obesity;

import java.util.Random;


/**
 * SelectDisease
 */
public class SelectDisease {
    private Random r = new Random();
    private final int AMOUNT_OF_DISEASES = 4;

    public void selectDisease(Patient patient){
        int disease = r.nextInt(AMOUNT_OF_DISEASES)+1;

        switch(disease){
            case 1:
                ChronicHeartDisease chd = new ChronicHeartDisease();
                patient.setClassifier("" + disease);
                chd.setSymptoms(patient);
                break;
            case 2:
                Diabetes d = new Diabetes();
                patient.setClassifier("" + disease);
                d.setSymptoms(patient);
                break;
            
            case 3:
                Normal n = new Normal();
                patient.setClassifier("" + disease);
                n.setSymptoms(patient);
                break;

            case 4:
                Obesity o = new Obesity();
                patient.setClassifier("" + disease);
                o.setSymptoms(patient);
                break;
        }
    }
    
}