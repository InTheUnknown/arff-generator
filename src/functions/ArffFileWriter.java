package functions;
/**
 * FileWriter
 */

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class ArffFileWriter {

    public boolean writeToFile(String data, boolean training){
        File f;
        if(training){
            f= new File("train.arff");
        }else{
            f= new File("test.arff");
        }
        try{
            if(!f.exists()){
                f.createNewFile();
            }
            else{
                f.delete();
                f.createNewFile();
            }

            PrintWriter pw = new PrintWriter(f);

            pw.write(data);
            pw.close();

            return true;
        }
        catch(IOException e){
            return false;
        }
    }
}