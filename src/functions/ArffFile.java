package functions;

import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;
import java.text.ParseException;
import java.util.ArrayList;
import weka.core.Utils;

import dataobjects.Patient;

/**
 * Class for building the ARFF file before writing to file
 */
public class ArffFile {
    private ArrayList<Attribute> attrs = new ArrayList<Attribute>();
    private ArrayList<String> gender = new ArrayList<String>();
    private ArrayList<String> present = new ArrayList<String>();
    private ArrayList<String> classifier = new ArrayList<String>();
    private Instances data;
    private double[] values;
    private Utils u;

    public ArffFile() {
        gender.add("Male");
        gender.add("Female");
        gender.add("Other");
        present.add("Yes");
        present.add("No");
        classifier.add("1");
        classifier.add("2");
        classifier.add("3");
        classifier.add("4");

        attrs.add(new Attribute("DOB", "yyyy-MM-dd"));
        attrs.add(new Attribute("BMI"));
        attrs.add(new Attribute("Gender", gender));
        attrs.add(new Attribute("Blood Pressure Systolic"));
        attrs.add(new Attribute("Blood Pressure Diastolic"));
        attrs.add(new Attribute("Glucose"));

        attrs.add(new Attribute("Shortness of Breath", present));
        attrs.add(new Attribute("Fatigue", present));
        attrs.add(new Attribute("Swelling in Legs", present));
        attrs.add(new Attribute("Rapid or Irregular Heartbeat", present));
        attrs.add(new Attribute("Numbness", present));
        attrs.add(new Attribute("Frequent Urination", present));
        attrs.add(new Attribute("Classifier", classifier));

        data = new Instances("AI", attrs, 0);
    }

    public boolean inputPatient(Patient patient, boolean isTraining){
        values = new double[data.numAttributes()];
        u = new Utils();

        try {
            values[0] = data.attribute(0).parseDate(patient.getDOB());
        } catch (ParseException e) {
            return false;
        }
        values[1] = patient.getBMI();
        values[2] = gender.indexOf(patient.getGender());
        values[3] = patient.getBPSys();
        values[4] = patient.getBPDia();
        values[5] = patient.getGlucose();
        values[6] = present.indexOf(patient.isShortOfBreath());
        values[7] = present.indexOf(patient.isFatigued());
        values[8] = present.indexOf(patient.isSwollen());
        values[9] = present.indexOf(patient.hasIrregularRapidHeartBeat());
        values[10] = present.indexOf(patient.isNumb());
        values[11] = present.indexOf(patient.isUrinatingFrequently());
        if(isTraining){
            values[12] = classifier.indexOf(patient.getClassifier());
        }
        else{
            values[12] = u.missingValue();
        }
        

        data.add(new DenseInstance(1.0, values));

        return true;
    }

    public boolean writeToFile(boolean training){
        ArffFileWriter afw = new ArffFileWriter();

        return afw.writeToFile(data.toString(), training);
    }

    public String getData(){
        return data.toString();
    }
}