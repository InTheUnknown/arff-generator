package dataobjects;

/**
 * patient
 */
public class Patient {
    //Patient Attributes
    private String dob;
    private int bmi;
    private String gender;
    private int bpSystolic;
    private int bpDiastolic;
    private int glucose;
    //Boolean Symptoms
    private String shortnessOfBreath = "No";
    private String fatigue = "No";
    private String swellingOfLegs = "No";
    private String rapidIrregularHeartBeat = "No";
    private String numbness = "No";
    private String frequentUrination = "No";
    //Classifier
    private String classifier="0";

    //Constructors
    public Patient() { }
    public Patient(String dob, String gender) {
        this.dob = dob;
        this.gender = gender;
    }

    //Getters
    public String getDOB(){
        return dob;
    }

    public int getBMI(){
        return bmi;
    }

    public String getGender(){
        return gender;
    }

    public int getBPSys(){
        return bpSystolic;
    }

    public int getBPDia(){
        return bpDiastolic;
    }

    public int getGlucose(){
        return glucose;
    }

    public String isShortOfBreath(){
        return shortnessOfBreath;
    }

    public String isFatigued(){
        return fatigue;
    }

    public String isSwollen(){
        return swellingOfLegs;
    }
    
    public String hasIrregularRapidHeartBeat(){
        return rapidIrregularHeartBeat;
    }

    public String isNumb(){
        return numbness;
    }

    public String isUrinatingFrequently(){
        return frequentUrination;
    }

    public String getClassifier(){
        return classifier;
    }

    //Setters
    public void setBMI(int bmi){
        this.bmi = bmi;
    }

    public void setBP(int sys, int dia){
        this.bpSystolic = sys;
        this.bpDiastolic = dia;
    }

    public void setGlucose(int glucose){
        this.glucose = glucose;
    }

    public void setShortnessOfBreath(String soh){
        this.shortnessOfBreath = soh;
    }

    public void setFatigues(String fatigued){
        this.fatigue = fatigued;
    }

    public void setSwollen(String swollen){
        this.swellingOfLegs = swollen;
    }

    public void setIrregularRapidHeartBeat(String rapidIrregularHeartBeat){
        this.rapidIrregularHeartBeat = rapidIrregularHeartBeat;
    }

    public void setNumbness(String numb){
        this.numbness = numb;
    }

    public void setUrinatingFrequently(String frequentUrination){
        this.frequentUrination = frequentUrination;
    }

    public void setClassifier(String classifier){
        this.classifier = classifier;
    }
}