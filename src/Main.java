import dataobjects.Patient;
import functions.GenerateValues;
import functions.ArffFile;
import functions.SelectDisease;
import java.util.Scanner;

/**
 * Main
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArffFile af = new ArffFile();
        GenerateValues gv = new GenerateValues();
        int numberOfRecords = 0;
        int option = 0;
        boolean isTraining = true;

        System.out.println("Select one of these options:");
        System.out.println("\t1.\tGenerate Training Set");
        System.out.println("\t2.\tGenerate Testing Set");
        System.out.print("Option: ");
        option = sc.nextInt();

        switch(option){
            case 1:
                isTraining = true;
                break;
            case 2:
                isTraining = false;
                break;
        }

        System.out.print("Enter number of records to generate: ");
        numberOfRecords = sc.nextInt();

        for(int i = 0; i < numberOfRecords; i++){
            Patient patient = new Patient(gv.generateDOB(), gv.generateGender());
            SelectDisease sd = new SelectDisease();

            sd.selectDisease(patient);

            af.inputPatient(patient, isTraining);
        }

        af.writeToFile(isTraining);
        sc.close();

    }
}