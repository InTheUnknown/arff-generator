package diseases;

import dataobjects.Patient;
import functions.GenerateValues;

/**
 * High BMI
 */
public class Obesity implements IDisease{
    private GenerateValues gv = new GenerateValues();

    @Override
    public void setSymptoms(Patient patient) {
        patient.setBMI(gv.generateBMI(30, 42));
        patient.setBP(gv.generateBPSys(80, 139), gv.generateBPDia(40, 89));
        patient.setGlucose(gv.generateGlucose(4, 10));

        patient.setShortnessOfBreath(gv.generateSymptomMajor());
        patient.setSwollen(gv.generateSymptomMinor());
        patient.setIrregularRapidHeartBeat(gv.generateSymptomMinor());
        patient.setFatigues(gv.generateSymptomMinor());

        patient.setUrinatingFrequently(gv.generateSymptomMinor());
        patient.setNumbness(gv.generateSymptomMinor());
    }


    
}