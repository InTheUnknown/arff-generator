package diseases;

import dataobjects.Patient;
import functions.GenerateValues;

/**
 * Normal ranges, symptoms are all NO
 */
public class Normal implements IDisease{
    private GenerateValues gv = new GenerateValues();
    
    @Override
    public void setSymptoms(Patient patient){
        patient.setBMI(gv.generateBMI(18, 29));
        patient.setBP(gv.generateBPSys(80, 120), gv.generateBPDia(40, 80));
        patient.setGlucose(gv.generateGlucose(4, 6));

        patient.setShortnessOfBreath(gv.generateSymptomMinor());
        patient.setSwollen(gv.generateSymptomMinor());
        patient.setIrregularRapidHeartBeat(gv.generateSymptomMinor());
        patient.setFatigues(gv.generateSymptomMinor());

        patient.setUrinatingFrequently(gv.generateSymptomMinor());
        patient.setNumbness(gv.generateSymptomMinor());
    }
    
}