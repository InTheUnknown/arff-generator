package diseases;

import dataobjects.Patient;
import functions.GenerateValues;

/**
 * High glucose, frequent urination, numbness
 */
public class Diabetes implements IDisease{
    private GenerateValues gv = new GenerateValues();
    
    @Override
    public void setSymptoms(Patient patient){
        patient.setBMI(gv.generateBMI(12, 42));
        patient.setBP(gv.generateBPSys(80, 120), gv.generateBPDia(40, 80));
        patient.setGlucose(gv.generateGlucose(7, 18));

        patient.setShortnessOfBreath(gv.generateSymptomMinor());
        patient.setSwollen(gv.generateSymptomMinor());
        patient.setIrregularRapidHeartBeat(gv.generateSymptomMinor());
        patient.setFatigues(gv.generateSymptomMinor());

        patient.setUrinatingFrequently(gv.generateSymptomMajor());
        patient.setNumbness(gv.generateSymptomMajor());
    }
    
}