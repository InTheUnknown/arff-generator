package diseases;

import dataobjects.Patient;

/**
 * IDisease
 */
public interface IDisease {
    public void setSymptoms(Patient patient);
}