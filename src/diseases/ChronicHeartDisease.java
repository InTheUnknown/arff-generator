package diseases;

import dataobjects.Patient;
import functions.GenerateValues;

/**
 * Swelling, Rapid/irregular heartbeat, high blood pressure
 */

public class ChronicHeartDisease implements IDisease{
    private GenerateValues gv = new GenerateValues();
    
    @Override
    public void setSymptoms(Patient patient){
        patient.setBMI(gv.generateBMI(12, 42));
        patient.setBP(gv.generateBPSys(125, 180), gv.generateBPDia(80, 120));
        patient.setGlucose(gv.generateGlucose(4, 10));

        patient.setShortnessOfBreath(gv.generateSymptomMajor());
        patient.setSwollen(gv.generateSymptomMajor());
        patient.setIrregularRapidHeartBeat(gv.generateSymptomMajor());
        patient.setFatigues(gv.generateSymptomMajor());

        patient.setUrinatingFrequently(gv.generateSymptomMinor());
        patient.setNumbness(gv.generateSymptomMinor());
    }
}